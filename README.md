
Setup guide:
1. Add following line into /etc/rc.local
   nohup runuser -l pi -c '/usr/bin/python /home/pi/scripts/power_controller.py' &
2. mkdir -p /opt/3dp_power_controller/ && touch /opt/3dp_power_controller/controller.log
3. chmod pi. /opt/3dp_power_controller/controller.log
4. Generate new API token in UI and replace the value of octo_api_key variable in power_controller.py
5. install "GCODE system commands" plugin
6. add "GCODE system commands" config: 
   OCTO100: bash -c "nc -U -q0 /opt/3dp_power_controller/printer_power_controller.sock <<< 'print_end_off:900'”



OCTO100: bash -c "nc -U -q0 /opt/3dp_power_controller/printer_power_controller.sock <<< 'print_end_off:900'"
