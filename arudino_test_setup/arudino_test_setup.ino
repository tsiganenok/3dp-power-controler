int RL_pin = 7;
int BT_pin = 8;
int LD_pin = 9;
bool RL_state = LOW;

void setup() {
  Serial.begin(9600);
  pinMode(RL_pin, OUTPUT);
  pinMode(LD_pin, OUTPUT);
  pinMode(BT_pin, INPUT);
}

void loop() {
  if (digitalRead(BT_pin) == HIGH) {
    RL_state = !RL_state;
    digitalWrite(RL_pin, RL_state);
    if (RL_state == HIGH) {
      digitalWrite(LD_pin, LOW);
    } else {
      digitalWrite(LD_pin, HIGH);
    }
    delay(1000);
  }
}
