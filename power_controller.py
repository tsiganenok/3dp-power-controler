#!/usr/bin/python

import RPi.GPIO as GPIO
import threading
import socket
import sys
import os
import logging
import httplib
import json
from time import time, sleep

octo_api_key      = "9612B6BC6B4C4700B619F61673181497"
default_sleep     = 10 # default amout of time we'll wait before turning the machine off (for cooling)
pwr_button_pin    = 6  # BCM Power button pin
pwr_led_pin       = 26 # BCM Power LED pin (Transistor base)
relay_pin         = 18 # BCM Relay pin 18
lights_button_pin = 20 # BCM Lights button pin
printer_state     = False # default value.
lt_state          = False 
base_dir          = "/opt/3dp_power_controller/"
log_file          = base_dir + "controller.log"
socket_file       = base_dir + "printer_power_controller.sock" # The only comunication channel betwee us and Octoprint.
socket_off_file   = base_dir + "printer_power_controller.state" # In order to disable automatic shutdown for print, put "off" into this file (Default is "on")
log_format        = "%(levelname)s %(asctime)s %(message)s"
long_press_sec    = 3 # length of long power button press in seconds
tool_temp_threshold = 80 # Above this temperature safe shutdown will be disabled

def setup():
    logging.basicConfig(filename=log_file, format=log_format, level=logging.DEBUG)

    logging.info("Starting up, setting pins")
    logging.info("Pin %d - Power button" % (pwr_button_pin))
    logging.info("Pin %d - Power button LED" % (pwr_led_pin))
    logging.info("Pin %d - Relay" % (relay_pin))
    logging.info("Pin %d - Lights button" % (lights_button_pin))

    
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pwr_button_pin, GPIO.IN) # pull-down resistor should be in place...
    GPIO.setup(lights_button_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(pwr_led_pin, GPIO.OUT, initial=GPIO.HIGH)
    GPIO.setup(relay_pin, GPIO.OUT, initial=GPIO.LOW)
        
    socket_thread = threading.Thread(target=setup_socket)
    socket_thread.start()
        
def socket_off(data):
    slp_time = default_sleep
    
    if len(data) > 1: # we have a timeout provided by requester
        try: 
            slp_time = int(data[1])
        except:
            logging.info("Provided sleeptime isn't integer...")
        
    try:
        logging.info("Sleeping %d" % slp_time)
        sleep(float(slp_time))
        
    except Exception as ex:
        logging.error("Sleep failed, sleeping default: %d" % default_sleep)
        logging.error(str(ex))
        sleep(default_sleep)
    
    # Check the status of "Disable on print end" option because it could be changed while 
    # we were asleep.
    status = get_socket_off_status()
    if status:
        return off()
    else:
        logging.info("Status has been changed while I was asleep, will NOT shutdown")
        return True
    
def get_socket_off_status():
    try:
        f = open(socket_off_file, "r")
        content = f.read().strip()
        f.close()
        
        if content == 'off':
            logging.info("Shutdown at print finish is disabled, check: %s" % socket_off_file)
            return False
            
    except Exception as ex:
        logging.error("Failed to get the print_end_shutdown feature state")
        logging.error(str(ex))
        pass
        
    return True
        
def setup_socket():
    logging.info("Starting socket thread")
    try:
        os.unlink(socket_file)
    except OSError:
        if os.path.exists(socket_file):
            raise
            
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.bind(socket_file)
    sock.listen(1)
    
    while True:
        connection, client_address = sock.accept()
        try:
            while True:
                data = connection.recv(20).strip()
                
                if data:
                    logging.info("Got a command from socket: %s" % data)
                    # Session will stuck till the end of this block execution 
                    # (which could include sleeping for a while ..) 
                    # if we don't close it here.
                    connection.close()
                    data_arr = data.split(':')
                    
                    if data_arr[0] == "on":
                        printer_state = on()
                        
                        
                    elif data_arr[0] == "off":
                        printer_state = off()
                        
                        
                    elif data_arr[0] == "print_end_off":
                        status = get_socket_off_status()
                        
                        if status:
                            printer_state = socket_off(data_arr)
                            
                        else:
                            break
                            
                    else:
                        logging.warning("Unknown command")
                        
                    break
                        
                else:
                    break

        finally:
            connection.close()
    
def is_safe():
    printer_state = get_printer_state()
    
    # Octo isn't connected to printer now.
    if printer_state == "Printer is not operational":
        return True
        
    # Not safe - printer is printing now.
    if printer_state["state"]["text"] == "Printing":
        return False
    
    for key in printer_state["temperature"]:
        if key == "bed":
            continue
        
        if key.startswith("tool", 0):
            if printer_state["temperature"][key]["actual"] > tool_temp_threshold:
                # One of tools's temperature is to high, not safe to turn off now.
                return False
                
    return True
    
def off(how = "safe"):
    try:
        if not how == "notsafe": # if how is default or anything except "notsafe"
            if not is_safe():
                logging.warning("Not safe to turn printer off now.")
                led_blink(pwr_led_pin, 3)
                return True
            
        logging.info("Printer goes off")
        GPIO.output(relay_pin, GPIO.LOW)
        GPIO.output(pwr_led_pin, GPIO.HIGH)
        return False
    except Exception as ex:
        logging.error(str(ex))
    
def on():
    logging.info("Printer goes on")
    GPIO.output(relay_pin, GPIO.HIGH)
    GPIO.output(pwr_led_pin, GPIO.LOW)
    return True
    
def led_blink(led, count):
    for i in range(count):
        GPIO.output(led, GPIO.HIGH)
        sleep(0.3)
        GPIO.output(led, GPIO.LOW)
        sleep(0.3)
    
def get_printer_state():
    logging.debug("Retrieving printer state")
    
    try:
        headers = {"X-Api-Key": octo_api_key}
        conn    = httplib.HTTPConnection("localhost")
        conn.request("GET", "/api/printer", "", headers)
        resp = conn.getresponse()
        
        if resp.status == 200:
            data = json.loads(resp.read())
            logging.debug(data)
            return data
        else:
            data = resp.read()
            logging.debug("Got unusual state for printer status")
            logging.debug(resp.status)
            logging.debug(resp.reason)
            logging.debug(data)
            return data
    except Exception as ex:
        logging.error("Error ocured while checking printer status")
        logging.error(str(ex))
    
setup()

logging.info("Starting button listener")
counter    = 0
lt_counter = 0
start_time = 0

while True:
 
    # Power button...
    if GPIO.input(pwr_button_pin) == GPIO.HIGH:
        # logging.info("HIGH")
        counter += 1
        if counter == 2:
            start_time = time()
                    
        if counter > 2:
            if time() >= start_time + long_press_sec:
                logging.info("Long press recognized")
                printer_state = off("notsafe")
                counter    = 0
                start_time = 0
                sleep(1)
            
    else:
        # logging.info("LOW")
        if counter >= 100:
            logging.info("Short press recognized")
                        
            if printer_state == True:
                printer_state = off()
                    
            else:
                printer_state = on()
                
            counter    = 0
            start_time = 0
            sleep(1)
            
        else:
            counter = 0
            
    # Lights button...
    if GPIO.input(lights_button_pin) == GPIO.HIGH:
        lt_counter += 1
        
        if lt_counter >= 100:
            if lt_state == False:
                logging.info("Turning lights On")
                lt_state = True
            else: 
                logging.info("Turning lights Off")
                lt_state = False
                
